INSERT INTO q2pds77292hksd8p.CATEGORY VALUES
	(1,'Console',NULL),
	(2,'Jeux',NULL),
	(3,'Accessoire',NULL);

INSERT INTO q2pds77292hksd8p.PLATEFORM VALUES
	(1,"Wii",NULL,NULL),
	(2,"3DS",NULL,NULL),
	(3,"Megadrive",NULL,NULL),
	(4,"Nintendo 64", NULL,NULL);

INSERT INTO q2pds77292hksd8p.MODEL VALUES
	(1,3,1,"Wiimote",NULL,NULL),
	(2,3,1,"Wiimote plus",NULL,NULL),
	(3,2,1,"Super Mario Galaxy",NULL,NULL),
	(4,2,1,"Super Mario Galaxy 2",NULL,NULL),
	(5,1,1,"Wii Mini",NULL, NULL),
	(6,2,4,"F-ZERO X",NULL,NULL),
	(7,1,2,"3DS XL",NULL,NULL),
	(8,1,2,"New 3DS XL",NULL, NULL),
	(9,3,3,"Manette 3 boutons",NULL, NULL),
	(10,3,3,"Manette 6 boutons",NULL, NULL),
	(11,1,1,"Wii Classique",NULL,NULL);
