-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 14 Mai 2019 à 23:18
-- Version du serveur :  10.3.13-MariaDB-2
-- Version de PHP :  7.2.17-0ubuntu0.19.04.1


-- SET time_zone = "+02:00";



-- Base de données :  `collectionjv`

-- --------------------------------------------------------
CREATE OR REPLACE TABLE q2pds77292hksd8p.PLATEFORM (
  IDP INT UNSIGNED NOT NULL AUTO_INCREMENT,
  WORDING varchar(30) NOT NULL,
  DESCRIPTION text(1024) DEFAULT NULL,
  PICTURELINK varchar(500) DEFAULT NULL,
  PRIMARY KEY (IDP)
) ENGINE=InnoDB DEFAULT CHARSET utf8;
--
-- Structure de la table `CATEGORY`
--

CREATE OR REPLACE TABLE q2pds77292hksd8p.CATEGORY(
  IDC INT UNSIGNED NOT NULL AUTO_INCREMENT,
  WORDING varchar(30) NOT NULL,
  DESCRIPTION varchar(1024) DEFAULT NULL,
  PRIMARY KEY(IDC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- Structure de la table `USER`
--

CREATE OR REPLACE TABLE q2pds77292hksd8p.USER (
  ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PSEUDO varchar(128) NOT NULL,
  EMAIL varchar(128) NOT NULL,
  NAME varchar(30) DEFAULT NULL,
  FIRSTNAME varchar(50) DEFAULT NULL,
  PASSWORD varchar(255) NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
--
-- Structure de la table `COLLECTION`
--

CREATE OR REPLACE TABLE q2pds77292hksd8p.COLLECTION (
  IDC INT UNSIGNED NOT NULL AUTO_INCREMENT,
  IDU INT UNSIGNED NOT NULL,
  WORDING varchar(30) NOT NULL,
  PRIMARY KEY (IDC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MODEL`
--

CREATE OR REPLACE TABLE q2pds77292hksd8p.MODEL (
  IDM INT UNSIGNED NOT NULL AUTO_INCREMENT,
  IDC INT UNSIGNED NOT NULL,
  IDP INT UNSIGNED NOT NULL,
  WORDING varchar(30) NOT NULL,
  DESCRIPTION text(1024) DEFAULT NULL,
  PICTURELINK varchar(500) DEFAULT NULL,
  PRIMARY KEY (IDM)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `NUMBER_MODEL`
--

CREATE OR REPLACE TABLE q2pds77292hksd8p.NUMBER_MODEL (
  IDC INT UNSIGNED NOT NULL,
  IDM INT UNSIGNED NOT NULL,
  QUANTITY INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

CREATE OR REPLACE TABLE q2pds77292hksd8p.COOKIETOKEN(
  IDU INT UNSIGNED NOT NULL,
  TOKEN varchar(255) NOT NULL,
  DATEEXPI TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- --------------------------------------------------------

--

-- --------------------------------------------------------

--
-- Structure de la table `PART_MODEL`

-- --------------------------------------------------------

--
-- Structure de la table `PLATEFORM`
--



-- --------------------------------------------------------

-- --------------------------------------------------------

--


--

-- Contenu de la table `USER`
--
--
-- Index pour les tables exportées
--

--
-- Index pour la table `CATEGORY`
--

--
-- Index pour la table `COLLECTION`
--
ALTER TABLE COLLECTION
  ADD KEY `k_collection` (IDU);

--
-- Index pour la table `MODEL`
--
ALTER TABLE MODEL
  ADD KEY k_model_plateform (IDP),
  ADD KEY k_model_category (IDC);

--


--
-- Index pour la table `PLATEFORM`



-- Index pour la table `USER`
--
ALTER TABLE USER
  ADD UNIQUE KEY (PSEUDO),
  ADD UNIQUE KEY (EMAIL);



ALTER TABLE NUMBER_MODEL
ADD PRIMARY KEY (`IDC`,IDM);


ALTER TABLE COOKIETOKEN
ADD PRIMARY KEY (IDU);
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `COLLECTION`
--
ALTER TABLE `COLLECTION`
  ADD CONSTRAINT `fk_collection` FOREIGN KEY (`IDU`) REFERENCES `USER` (`ID`);

--
-- Contraintes pour la table `MODEL`
--
ALTER TABLE `MODEL`
  ADD CONSTRAINT fk_model_category FOREIGN KEY (`IDC`) REFERENCES `CATEGORY` (`IDC`),
  ADD CONSTRAINT fk_model_plateform FOREIGN KEY (`IDP`) REFERENCES `PLATEFORM` (`IDP`);

--
-- Contraintes pour la table `NUMBER_MODEL`
--
ALTER TABLE `NUMBER_MODEL`
  ADD CONSTRAINT `fk_n_model_model` FOREIGN KEY (`IDM`) REFERENCES `MODEL` (`IDM`),
  ADD CONSTRAINT `fk_n_model_collection` FOREIGN KEY (`IDC`) REFERENCES `COLLECTION` (`IDC`);


ALTER TABLE COOKIETOKEN
  ADD CONSTRAINT fk_cookietoken_user FOREIGN KEY (IDU) REFERENCES USER (ID);

--
-- Trigger
--

CREATE TRIGGER delete_token_on_delete
AFTER DELETE ON COOKIETOKEN
FOR EACH ROW 
  DELETE FROM COOKIETOKEN WHERE  OLD.DATEEXPI <= NOW();

CREATE EVENT delete_token_every_12h
ON SChedule EVERY 12 HOUR
DO
DELETE FROM COOKIETOKEN WHERE DATEEXPI <= NOW(); 



/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
