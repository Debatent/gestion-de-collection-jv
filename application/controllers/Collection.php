<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collection extends MY_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('collection_model');
		$this->load->model('profile_model');
		$this->default_data['title']="";

 		if (!$this->default_data['isloggedin']){
			redirect('/login','location',401);
		}
	}
	public function index(){
		show_404();
	}

	public function show($idcollection){
		$data = $this->default_data;

		$proprietary = $this->profile_model->get_proprietary($idcollection);
		
		if(!($proprietary['IDU'] === $data['userconnected'])){
				show_error("Vous n'avez pas la permission d'accéder à cette page",403);
		}
		$name = $this->collection_model->get_collection_name($idcollection);
		$data['title'] = $name['WORDING'];
		$data['query'] = $this->collection_model->get_element_collection($idcollection);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('collection/show',$data);
		$this->load->view('templates/footer');
	}

	public function modify_object($idcollection, $idelement, $number){
		$data = $this->default_data;
		
		$proprietary = $this->profile_model->get_proprietary($idcollection);
		
		if(!($proprietary['IDU'] === $data['userconnected'])){
				show_error("Vous n'avez pas la permission d'accéder à cette page",403);
		}
		if($number<=0){
			$this->collection_model->delete($idcollection,$idelement);
		}
		else{
			$this->collection_model->modify_element_collection($idcollection,$idelement, $number);
		}
		$data['query'] = $this->collection_model->get_element_collection($idcollection);
		$name = $this->collection_model->get_collection_name($idcollection);
		$data['title'] = $name['WORDING'];
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('collection/show',$data);
		$this->load->view('templates/footer');
	}

	public function delete($idcollection, $idelement){
		$data = $this->default_data;
		$proprietary = $this->profile_model->get_proprietary($idcollection);
	
		if(!($proprietary['IDU'] === $data['userconnected'])){
			show_error("Vous n'avez pas la permission d'accéder à cette page",403);
		}
		$this->collection_model->delete($idcollection,$idelement);
		$name = $this->collection_model->get_collection_name($idcollection);
		$data['title'] = $name['WORDING'];
		$data['query'] = $this->collection_model->get_element_collection($idcollection);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('collection/show',$data);
		$this->load->view('templates/footer');
	}
}