<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller{

		public function __construct(){

				parent::__construct();

				$this->default_data['title']="Connexion";
				$this->default_data['subtitle']= 'Veuillez entrer vos identifiants';
				$this->default_data['errorLogin'] = '';
				if ($this->default_data['isloggedin']){
					redirect('/profile','location',401);
				}
		}



		public function index(){
				$data = $this->default_data;
				$this->load->view('templates/header', $data);
				$this->load->view('templates/menu', $data);
		    $this->load->view('auth/login', $data);
		    $this->load->view('templates/footer');
		}

		public function confirm(){
				$data = $this->default_data;
				$data['errorLogin'] = "Connexion échoué: Le nom d'utilisateur ou le mot de passe est invalide";
				$this->load->model('login_model');


				$rules = array(
        array(
                'field' => 'pseudo',
                'label' => 'Pseudonyme',
                'rules' => array(
												'required',
												'max_length[128]')
				),
        array(
                'field' => 'pswd',
                'label' => 'Mot de Passe',
                'rules' => array(
												'required',
												'max_length[72]')

				));



					$this->form_validation->set_rules($rules);

					//test the form
					if ($this->form_validation->run() === FALSE){
							//reload the page with old info
							$this->load->view('templates/header', $data);
							$this->load->view('templates/menu', $data);
							$this->load->view('auth/login',$data);
        			$this->load->view('templates/footer');
					}
					else{
							$row = $this->login_model->get_user_info();

							if (password_verify($this->input->post('pswd'),$row['PASSWORD'])){

									$save['token'] =  $this->security->get_random_bytes(72);
									$save['token'] = password_hash($save['token'],PASSWORD_DEFAULT);
									$save['pseudo'] = $row['PSEUDO'];
									$this->cookie_model->save_cookie_token($save);


									$cookievalue = array(
        							'name'   => 'cookie64',
        							'value'  => $save['token'],
        							'expire' => '86400',
        							'domain' => 'collection64.herokuapp.com',
        							'path'   => '/',
        							'prefix' => '',
        							'secure' => TRUE,
											'httponly'=> TRUE);
									set_cookie($cookievalue);
									$this->load->view('auth/success_login');
							}
							else{
									$this->load->view('templates/header', $data);
									$this->load->view('templates/menu', $data);
									$this->load->view('auth/login',$data);
									$this->load->view('templates/footer');

							}
					}


		}
}
