<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	

	public function index()
	{
		$data = $this->default_data;
		$data['title'] = "Accueil";

		$this->load->view('templates/header',$data);
		$this->load->view('templates/menu',$data);
		$this->load->view('welcome',$data);
		$this->load->view('templates/footer',$data);
	}
	public function search(){
		$data = $this->default_data;
		$data['title']='Recherche';
		$data['subtitle']='Résultat';
		$this->load->model('welcome_model');

		$data['query'] = $this->welcome_model->search();
		$this->load->view('templates/header',$data);
		$this->load->view('templates/menu',$data);
		$this->load->view('search_result',$data);
		$this->load->view('templates/footer',$data);
	}
	public function log_out(){
		$data = $this->default_data;
		delete_cookie("cookie64");
		$this->cookie_model->delete_cookie_token($data['userconnected']);
		$this->load->view('auth/success_login');
	}
}
