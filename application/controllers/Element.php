<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Element extends MY_Controller{

		public function __construct(){
			parent::__construct();

			$this->load->model('element_model');

			$this->default_data['title']="";
		}

		public function index(){
				show_404();
		}

		public function show($idproduct){
				$this->load->model('profile_model');
				$data = $this->default_data;
				$query = $this->element_model->show($idproduct);
				$data['title']=$query['WORDING'];
				$data['query']=$query;
				$data['collection']='';
				if ($data['isloggedin']){
					$data['collection'] = $this->profile_model->get_user_collection($data['userconnected']);

				}

				$this->load->view('templates/header', $data);
				$this->load->view('templates/menu', $data);
				$this->load->view('element/index',$data);
				$this->load->view('templates/footer');
		}

		public function add($idproduct, $idcollection,$number){
				$this->load->model('profile_model');
				$this->load->model('collection_model');
				$data = $this->default_data;
				$proprietary = $this->profile_model->get_proprietary($idcollection);
		
				if(!($proprietary['IDU'] === $data['userconnected'])){
					show_error("Vous n'avez pas la permission d'accéder à cette page",403);
				}
				if(!is_numeric($number)){
					show_error("La quantité n'est pas un nombre",403);
				}
				$row=  $this->element_model->get_quantity( $idcollection,$idproduct);
				if (empty($row['QUANTITY'])){
					$this->element_model->add( $idcollection,$idproduct,$number);
				}
				else if($row['QUANTITY']<=0){
					$this->collection_model->delete($idcollection,$idelement);
				}
				else{
					$this->collection_model->modify_element_collection($idcollection,$idelement, $number);
				}
				$query = $this->element_model->show($idproduct);
				$data['title']=$query['WORDING'];
				$data['query']=$query;
				$data['collection']='';
				if ($data['isloggedin']){
					$data['collection'] = $this->profile_model->get_user_collection($data['userconnected']);

				}

				$this->load->view('templates/header', $data);
				$this->load->view('templates/menu', $data);
				$this->load->view('element/index',$data);
				$this->load->view('templates/footer');
		}
}
