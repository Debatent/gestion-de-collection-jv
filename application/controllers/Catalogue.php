<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogue extends MY_Controller{
		public function __construct(){

				parent::__construct();
				$this->load->model('catalogue_model');

				$this->default_data['title'] = "Catalogue";
				$this->default_data['plateformname'] = '';
		}

		public function index(){
				redirect('/', 'location', 301);

		}

		public function category ($idcategory = 1, $idplateform = NULL){
				$data = $this->default_data;
				switch ($idcategory) {
					case 1:
						$data['subtitle'] = "Console";
						break;
					case 2:
						$data['subtitle'] = "Jeux";
						break;
					case 3:
						$data['subtitle'] = "Accessoire";
						break;
					default:
						show_404();
						break;
				}

				if ($idplateform === NULL){
							$query = $this->catalogue_model->get_plateform($idcategory);
							$data['query'] = $query;
 							$this->load->view('templates/header', $data);
							$this->load->view('templates/menu', $data);
							$this->load->view('catal/show_catalogue',$data);
							$this->load->view('templates/footer');
				}

				else{
					$data['plateformname'] =$this->catalogue_model->get_plateform_name($idplateform)->WORDING;
					$query = $this->catalogue_model->get_element($idcategory,$idplateform);
					$data['query'] = $query;
					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu', $data);
					$this->load->view('catal/show_catalogue',$data);
					$this->load->view('templates/footer');
				}
		}
}
