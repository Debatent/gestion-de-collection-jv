<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller{

	public function __construct(){

				parent::__construct();

				$this->load->model('profile_model');

				$this->default_data['title']="Profil";
				if (!$this->default_data['isloggedin']){
					redirect('/login','location',401);
				}
		}

		public function index(){
			$data = $this->default_data;
			$info = $this->profile_model->get_user_info($data['userconnected']);
			$collection = $this->profile_model->get_user_collection($data['userconnected']);
			$data['info'] = $info;
			$data['collection'] = $collection;
			$this->load->view('templates/header', $data);
			$this->load->view('templates/menu', $data);
			$this->load->view('profile/index',$data);
			$this->load->view('templates/footer');
			
	}
	public function add_collection(){
			$data = $this->default_data;
			
			if (!($this->input->post('idu') === $data['userconnected'])){
					show_error("Vous n'avez pas la permission d'accéder à cette page",403);
			}
			else{
				$this->profile_model->create_collection();
				$info = $this->profile_model->get_user_info($data['userconnected']);
				$collection = $this->profile_model->get_user_collection($data['userconnected']);
				$data['info'] = $info;
				$data['collection'] = $collection;
				$this->load->view('templates/header', $data);
				$this->load->view('templates/menu', $data);
				$this->load->view('profile/index',$data);
				$this->load->view('templates/footer');
			}

	}
	public function delete_collection($idcollection){
			$data = $this->default_data;
			$proprietary = $this->profile_model->get_proprietary($idcollection);
			
			if(!($proprietary['IDU'] === $data['userconnected'])){
					show_error("Vous n'avez pas la permission d'accéder à cette page",403);
			}
			$this->profile_model->delete_collection($idcollection,$data['userconnected']);
			$info = $this->profile_model->get_user_info($data['userconnected']);
			$collection = $this->profile_model->get_user_collection($data['userconnected']);
			$data['info'] = $info;
			$data['collection'] = $collection;
			$this->load->view('templates/header', $data);
			$this->load->view('templates/menu', $data);
			$this->load->view('profile/index',$data);
			$this->load->view('templates/footer');

	}
	public function modify_collection($idcollection){
			$data = $this->default_data;
			$proprietary = $this->profile_model->get_proprietary($idcollection);
			
			if(!($proprietary['IDU'] === $data['userconnected'])){
					show_error("Vous n'avez pas la permission d'accéder à cette page",403);
			}
			$this->profile_model->modify_collection($idcollection,$data['userconnected']);
			$info = $this->profile_model->get_user_info($data['userconnected']);
			$collection = $this->profile_model->get_user_collection($data['userconnected']);
			$data['info'] = $info;
			$data['collection'] = $collection;
			$this->load->view('templates/header', $data);
			$this->load->view('templates/menu', $data);
			$this->load->view('profile/index',$data);
			$this->load->view('templates/footer');

	}


}
