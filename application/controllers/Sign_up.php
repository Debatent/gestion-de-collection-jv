<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign_up extends MY_Controller{
		public function __construct(){

				parent::__construct();

					$this->default_data['title'] = "Inscription";
					$this->default_data['subtitle'] = 'Veuillez entrer les champs ci dessous';
					if ($this->default_data['isloggedin']){
						redirect('/profile','location',401);
					}
		}


		public function index(){
				$data = $this->default_data;
				$this->load->view('templates/header', $data);
				$this->load->view('templates/menu', $data);
        $this->load->view('auth/sign_up', $data);
        $this->load->view('templates/footer');


    }

		public function confirm(){
				$data = $this->default_data;
				$this->load->model('sign_up_model');

				//verification of constraints
				$rules = array(
        array(
                'field' => 'pseudo',
                'label' => 'Pseudonyme',
                'rules' => array(
												'required',
												'is_unique[USER.PSEUDO]',
												'max_length[128]')
        ),
				array(
							'field' => 'email',
							'label' => 'Email',
							'rules' => array(
											'required',
											'valid_email',
											'is_unique[USER.EMAIL]',
											'max_length[128]'
											)
				),
        array(
                'field' => 'pswd',
                'label' => 'Mot de Passe',
                'rules' => array(
												'required',
												'min_length[10]',
												'max_length[72]')


        ),
        array(
                'field' => 'pswd_verif',
                'label' => 'Vérification de Mot de Passe',
                'rules' => array(
												'required',
												'matches[pswd]'
											)
        ),
				);



					$this->form_validation->set_rules($rules);

					//test the form
					if ($this->form_validation->run() === FALSE){
							//reload the page with old info
							$this->load->view('templates/header', $data);
							$this->load->view('templates/menu', $data);
        			$this->load->view('auth/sign_up',$data);
        			$this->load->view('templates/footer');
					}
					else{
							$passWordHash = password_hash($this->input->post('pswd'),PASSWORD_DEFAULT);
							$this->sign_up_model->set_user($passWordHash);
							$this->load->view('auth/success_sign_up');

					}
			}


}
