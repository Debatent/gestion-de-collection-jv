<?php
 	class Element_model extends CI_Model{
			public function __construct(){
					$this->load->database();
			}
			public function show($idproduct){
					$sql="SELECT M.IDM, M.WORDING, M.DESCRIPTION, P.WORDING AS PLATEFORMNAME, C.WORDING AS CATEGORYNAME FROM MODEL M 
					LEFT JOIN (PLATEFORM P, CATEGORY C) ON (M.IDP = P.IDP AND M.IDC = C.IDC) WHERE IDM = ?";
					$query = $this->db->query($sql, $idproduct);
					return $query->row_array();
			}
			public function add( $idcollection, $idproduct,$number){
					$info_element = array(
							'IDC' => $idcollection,
							'QUANTITY' => $number,
							'IDM' => $idproduct);
					$this->db->insert('NUMBER_MODEL', $info_element);
			}
			public function get_quantity( $idcollection,$idproduct){
				$sql="SELECT QUANTITY FROM NUMBER_MODEL WHERE IDC = ? AND IDM = ?";
				$query = $this->db->query($sql, array($idcollection, $idproduct));
					return $query->row_array();
			}
		}
