<?php
class Welcome_model extends CI_Model{
		public function __construct(){
				$this->load->database();
		}
		public function search(){
				$value = $this->input->get('search');
				$this->db->like('WORDING',$value);
				$query = $this->db->get('MODEL');
				return $query->result_array();
		}
}
