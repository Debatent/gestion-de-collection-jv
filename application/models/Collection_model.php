<?php
 	class Collection_model extends CI_Model{
			public function __construct(){
					$this->load->database();
			}
			public function get_element_collection($idcollection){
				$sql="SELECT nm.IDC,nm.IDM,nm.QUANTITY,m.WORDING FROM NUMBER_MODEL nm LEFT JOIN MODEL m ON nm.IDM = m.IDM 
				WHERE nm.IDC = ?";
				$query = $this->db->query($sql,$idcollection);
				return $query->result_array();
			}

			public function get_collection_name($idcollection){
				$sql="SELECT WORDING FROM COLLECTION 
				WHERE IDC = ?";
				$query = $this->db->query($sql,$idcollection);
				return $query->row_array();
			}
			
			public function modify_element_collection($idcollection,$idelement, $number){
					$this->db->where('IDC',$idcollection);
	 			 	$this->db->where('IDM',$idelement);
	 			 	$info_collection = array(
	 					 'QUANTITY' => $number);
	 			 $this->db->update('NUMBER_MODEL', $info_collection);


			}
			public function delete($idcollection,$idelement){
					$info_collection = array(
							'IDM' => $idelement,
							'IDC' => $idcollection);
					$this->db->delete('NUMBER_MODEL', $info_collection);
					}
				}
