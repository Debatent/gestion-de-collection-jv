<?php

class Cookie_model extends CI_Model{
		public function  __construct(){
				parent::__construct();
				$this->load->database();
		}

		public function verify_cookie_token($data){
			/**
			 *The data is the value of the cookie64
			 *Returns ID or NULL
			 */
			$this->db->select('IDU');
			$this->db->where('TOKEN', $data);
			$query = $this->db->get('COOKIETOKEN');
			$row = $query->row_array();
			
			return $row['IDU'];
		}

		public function save_cookie_token($data){
			$this->db->select('ID');
			$this->db->where('PSEUDO', $data['pseudo']);
			$query = $this->db->get('USER');
			$row = $query->row_array();
			$value = array(
				'IDU' => $row['ID'],
				'TOKEN' => $data['token'],
				'DATEEXPI'=>date("Ymdhis",time()+86400)
			);
			$this->db->replace('COOKIETOKEN', $value);

		}
		public function delete_cookie_token($idu){
			$sql="DELETE FROM COOKIETOKEN WHERE IDU = ?";
			$this->db->query($sql,$idu);
		}
	

}
