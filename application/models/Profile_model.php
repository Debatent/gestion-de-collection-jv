<?php
class Profile_model extends CI_Model{
		public function __construct(){
				$this->load->database();
		}
		public function get_user_info($userconnected){
				$sql = "SELECT PSEUDO, EMAIL FROM USER WHERE ID = ?";
				$query = $this->db->query($sql, $userconnected);
				return $query->row_array();
		}
		public function get_user_collection($userconnected){
				$sql = "SELECT * FROM COLLECTION WHERE IDU = ?";
				$query = $this->db->query($sql, $userconnected);
				return $query->result_array();
		}
		public function create_collection(){
				$info_collection = array(
						'IDU' => $this->input->post('idu'),
						'WORDING' => $this->input->post('wording'));
				
				$this->db->insert('COLLECTION', $info_collection);
		}
		public function delete_collection($idcollection,$iduser){
				$info_collection = array(
						'IDU' => $iduser,
						'IDC' => $idcollection);
				$element =array('IDC' => $idcollection);
				$this->db->delete('NUMBER_MODEL',$element);
				$this->db->delete('COLLECTION', $info_collection);
		}

		public function modify_collection($idcollection,$iduser){
			/**
			 *Change name of collection
			 */
			 $this->db->where('IDU',$iduser);
			 $this->db->where('IDC',$idcollection);
			 $info_collection = array(
					 'WORDING' => $this->input->post('wording'));
			 $this->db->update('COLLECTION', $info_collection);
		}
		public function get_proprietary($idcollection){
			$sql="SELECT IDU FROM  COLLECTION WHERE IDC = ?";
			$query = $this->db->query($sql,$idcollection);
			return $query->row_array();
		}
	}
