<?php
 	class Catalogue_model extends CI_Model{
			public function __construct(){
					$this->load->database();
			}
			public function get_plateform($data){
				/**
				 *return all plateform link to a category
				 *$data = id of the category
				 */

				$sql = "SELECT P.IDP AS ID, P.WORDING, P.PICTURELINK FROM PLATEFORM  P, MODEL M WHERE M.IDC = ? AND P.IDP = M.IDP GROUP BY ID ORDER BY P.WORDING";
				$query = $this->db->query($sql,$data);
				return $query->result_array();

			}
			public function get_element($category,$plateform){
				/**
				 *return all element link to this category and this plateform
				 *$category = id of the category
         *$plateform = id of the platform
				 */

				$sql = "SELECT IDM AS ID, WORDING, PICTURELINK  FROM MODEL WHERE IDC = ? AND IDP = ? ORDER BY WORDING";
				$query = $this->db->query($sql,array($category,$plateform));
				return $query->result_array();
			}

      public function get_plateform_name($idplateform){
        $sql = "SELECT WORDING  FROM PLATEFORM WHERE IDP = ? ";
        $query = $this->db->query($sql,$idplateform);
        return $query->row();
      }
}
