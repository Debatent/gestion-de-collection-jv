<?php
class MY_Controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->load->helper('cookie');
		$this->load->model('cookie_model');
		
		$cookie = get_cookie("cookie64");
		$userconnected = $this->cookie_model->verify_cookie_token($cookie);
		$this->default_data = array(
			'isloggedin' => !empty($userconnected),
			'userconnected'=> $userconnected);
	}
			

}
