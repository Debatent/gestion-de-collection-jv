<div class='container width:75%'>
<h1><?php echo $title; ?></h1>

<h2><?= $subtitle ?></h2>


<span class='error'><?= $errorLogin ?></span>

<?php echo form_open('login/confirm', 'class=form-group');?>

	<label for="pseudo">Pseudonyme</label>
	<input class="form-control" type="text" name="pseudo" value="<?php echo set_value('pseudo'); ?>" maxlength="128" required>
	<p id="msg_pseudo"></p>
	<label for="pswd">Mot de passe</label>
	<input class="form-control" type="password" name="pswd" maxlength="72" required>
	<p id="msg_pswd"></p>
	<button class="btn btn-primary" type='submit'>Envoyer</button>
</form>

<a href=<?php echo site_url('sign_up')?>>Inscription</a>
</div>
