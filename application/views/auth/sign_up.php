<div class='container width:75%'>
<h1><?php echo $title; ?></h1>

<h2><?= $subtitle ?></h2>


<?= validation_errors('<span class="error">','</span>') ?>
<!--add some attribute for javascript-->
<?php $attribute = array(
	'name'=> 'form1',
	'onsubmit'=>'return validateForm()'
	);
echo form_open('sign_up/confirm', $attribute);?>
	<div class='form-group'>
	<label for="pseudo">Pseudonyme</label>
	<input class="form-control" type="text" name="pseudo" value="<?php echo set_value('pseudo'); ?>" maxlength="128" required>
	<p id="msg_pseudo"></p>
	<label for="email">Adresse mail</label>
	<input class="form-control"type="email" name="email" value="<?php echo set_value('email'); ?>" maxlength="128"  required>
	<p id="msg_email"></p>
	<label for="pswd">Mot de passe</label>
	<input class="form-control"type="password" name="pswd" maxlength="72" required>
	<p>Le Mot de passe doit faire plus de 10 caractères</p>
	<p id="msg_pswd"></p>
	<p id="msg_lenght_min_pswd"></p>
	<label for="pswd_verif">Vérification du mot de passe</label>
	<input class="form-control" type="password" name="pswd_verif" maxlength="72" required>
	<p id="msg_pswd_verif"></p>
	<p id="msg_same_pswd"></p>
</div>
	<button type="submit" class="btn btn-primary">Envoyer</button>
</form>
</div>

<!--
<script>
	function lenghtMinPassword(){
		var x = document.forms['form1'];
		if (x.["pswd"].value.lenght <= 10){
			x.setCustomValidity("Le mot de passe doit faire plus de 10 caractères");
			x.getElementById("msg_lenght_min_pswd").innerHTML = x.validationMessage;

		}
		else{
			x.["pswd"].setCustomValidity("");
			x.getElementById("msg_lenght_min_pswd").innerHTML = x.validationMessage;


		}
	}

	//x = document.forms
	function samePassword(x){
		var x = document.forms['form1'];
		if !(x.["pswd"].value == x.["pswd_verif"].value){
			x.setCustomValidity("Les mots de passe ne sont pas les mêmes");
			x.getElementById("msg_same_pswd").innerHTML = x.["pswd"].validationMessage;

		}
		else{
			x.["pswd_verif"].setCustomValidity("");
			x.getElementById("msg_same_pswd").innerHTML = x.["pswd_verif"].validationMessage;

		}

	}


	function validateForm(){
		var x, y msgName;
		msgName = ["pseudo", "email", "pswd","pwsd_verif"];
		x = document.forms['form1'];
		for (y in msgName){
			if (!x.y.checkValidity()){
				x.getElementById("msg_"+y).innerHTML = x.y.validationMessage;
			}
			else{
				x.getElementById("msg_"+y).innerHTML = "";
			}
		}
		lengthMinPassword(x);
		samePassword(x);
	}


</script>
-->
