<div class='container width:75%'>
<h1><?= $title ?></h1>

<h2><?= $subtitle ?></h2>
<h3><?= $plateformname ?></h3>
<h4>Articles</h4>
<?php if(empty($query)):?>
  <p>Aucun article ne satisfait les critères</p>
<?php else:?>
<div class='row'>
<?php foreach ($query as $row):?>
  <?php $link =(current_url().'/'.$row['ID'])?>
  <a class="col-6 col-md-4 col-lg-3" href=<?php echo $link?>>
  <div class="card text-center" >
      <img src='
      <?php if (empty($row['PICTURELINK'])):?> http://hdimages.org/wp-content/uploads/2017/03/placeholder-image4.jpg
      <?php else: ?><?= $row['PICTURELINK']?>
      <?php endif;?>'
      class="card-img-top" alt=<?= $row['WORDING'] ?>>
        <h5 class='card-title'><?= $row['WORDING'] ?></h5>
  </div>
</a>
<?php endforeach;?>
</div>
<?php endif;?>
</div>
