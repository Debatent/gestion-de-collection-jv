<div class='container width:75%'>
    <h1><?php echo $title ?></h1>
    <div class='row col-12'>
            <img class='img-fluid max-width:100%' src='
                <?php if (empty($query['PICTURELINK'])):?> http://hdimages.org/wp-content/uploads/2017/03/placeholder-image4.jpg
                <?php else: ?><?= $query['PICTURELINK']?>
                <?php endif;?>'>
    </div>
    <div class='row col-12'>
        <div class='container'>
        <h2>Information</h2>
        <ul class="list-group-flush">
            <li class="list-group-item">Plateforme: <?=$query['PLATEFORMNAME']?></li>
            <li class="list-group-item">Catégorie: <?=$query['CATEGORYNAME']?></li>
        </ul>
        </div>
    </div>
    <h2>Ajout à une collection</h2>
    <?php if($isloggedin):?>
   
        <button class='btn btn-primary' data-toggle="modal" data-target="#addelement">Ajout</button>



        <div class="modal fade" id="addelement" tabindex="-1" role="dialog" aria-labelledby="addelementlabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addelementlabel">Veuillez sélectionner une collection</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!--
        <form class='group-form'>
        <input type='number' name='quantity' id='howmany' class='form-control' placeholder='Quantité' value='1'>
        </form>
                -->
        <?php foreach($collection as $row):?>
            <div class="card text-center"  >
            <a href="<?php echo site_url("element/add/".$query['IDM'].'/'.$row['IDC'].'/')?>1">
                <div class='card-body'>
                <h5 class='card-title'><?= $row['WORDING'] ?></h5>
                </div>
            </div>
        <?php endforeach ?>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary col-5" data-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
    <p>Vous n'etes pas connecté </p>
                <?php endif; ?>

</div>


