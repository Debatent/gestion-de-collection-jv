<div class="container width:75%">
<h1><?= $title?></h1>
<h2>Informations Personnelles</h2>
<div class='col-8'>
<ul class="list-group-flush">
	<li class="list-group-item">Pseudo: <?=$info['PSEUDO']?></li>
	<li class="list-group-item">Email: <?=$info['EMAIL']?></li>
</ul>
</div>
<!--show Collection-->
<h2>Collections</h2>
<?php if(empty($collection)):?>
  <p>Vous ne possédez aucune collection</p>
<?php else:?>
<div class='row'>
<?php foreach ($collection as $row):?>

<div class="card text-center col-6 col-md-4 col-lg-3 mb-3 "  >
    <a href=<?php echo site_url("collection/".$row['IDC'])?>>
        <div class='card-body'>
        <h5 class='card-title'><?= $row['WORDING'] ?></h5>
        </div>
      </a>
        <div class='card-footer'>
          <!--More option-->
          <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Options
          </button>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <?= form_open('profile/modify_collection/'.$row['IDC'],'class=group-form')?>
              <input class='dropdown-item border-top border-bottom' placeholder='Nouveau nom' type='text' name='wording' required>
              <button class="dropdown-item" type='input'>Changer nom</button>
              </form>
              <a class="dropdown-item" href="<?= site_url('profile/delete_collection/'.$row['IDC'])?>">Supprimer</a>
            </div>
          </div>
          </div>  
        
</div> 
<?php endforeach;?>
</div>
<?php endif;?>

	<button class="btn btn-primary" data-toggle="modal" data-target="#createcollection">Nouvelle collection</button>

<!-- Create collection -->
<div class="modal fade" id="createcollection" tabindex="-1" role="dialog" aria-labelledby="createcollectionlabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="asknamelabel">Veuillez donner un nom à la nouvelle collection</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?= form_open('profile/add_collection','class=group-form')?>
        <input type='hidden' name='idu' class='form-control' value='<?= $userconnected?>'>
        <input type='text' name='wording' class='form-control'>
        
      </div>
      <div class="modal-footer">
        <button type="input" class="btn btn-primary col-5">Créer</button>
        </form>
        <button type="button" class="btn btn-secondary col-5" data-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>



</div>
