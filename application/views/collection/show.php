<div class="container width:75%">
<h1><?= $title ?></h1>

<h2>Éléments de la collection</h2>

<?php if(empty($query)):?>
    <p>Cette collection est vide</p>
<?php else:?>
    
    <?php foreach($query as $row):?>
    <?php $link =(site_url('element/show/').$row['IDM'])?>
    <div class="card text-center col-6 col-md-4 col-lg-3" >
        <a href=<?php echo $link?>>
        <div class='card-body'>
            <img src='
            <?php if (empty($row['PICTURELINK'])):?> http://hdimages.org/wp-content/uploads/2017/03/placeholder-image4.jpg
            <?php else: ?><?= $row['PICTURELINK']?>
            <?php endif;?>'
            class="card-img-top" alt=<?= $row['WORDING'] ?>>
            <h5 class='card-title'><?= $row['WORDING'] ?></h5>
        </div>
        </a>
        <div class='card-foot'>
            <a class='btn btn-secondary' href="<?php echo site_url('collection/modify_object/'.$row['IDC'].'/'.$row['IDM'].'/'.($row['QUANTITY']+1))?>">+1</a>
            
            <a class='btn btn-secondary' href="<?php echo site_url('collection/modify_object/'.$row['IDC'].'/'.$row['IDM'].'/'.($row['QUANTITY']-1))?>">-1</a>
            <span>Qté: <?= $row['QUANTITY'] ?></span>
            <a class='btn btn-danger' href="<?php echo site_url('collection/delete/'.$row['IDC'].'/'.$row['IDM']) ?>">Supprimer</a>
            
            </div>

  </div>
      <?php endforeach; ?>
      <?php endif; ?>
</div>