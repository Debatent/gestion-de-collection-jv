<div class='container width:75%'>
<h1><?= $title ?></h1>

<h2><?= $subtitle ?></h2>

<?php if(empty($query)):?>
  <p>Aucun article ne correspond à la recherche</p>
<?php else:?>
<div class='row'>
<?php foreach ($query as $row):?>
  <?php $link =(site_url("element/show/".$row['IDM']))?>
  <a class="col-6 col-md-4 col-lg-3" href=<?php echo $link?>>
  <div class="card text-center" >
      <img src='
      <?php if (empty($row['PICTURELINK'])):?> http://hdimages.org/wp-content/uploads/2017/03/placeholder-image4.jpg
      <?php else: ?><?= $row['PICTURELINK']?>
      <?php endif;?>'
      class="card-img-top" alt=<?= $row['WORDING'] ?>>
        <h5 class='card-title'><?= $row['WORDING'] ?></h5>
  </div>
</a>
<?php endforeach;?>
</div>
<?php endif;?>
</div>
