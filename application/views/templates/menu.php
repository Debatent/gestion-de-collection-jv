<nav class="navbar sticky-top navbar-expand-md navbar-light bg-light">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  	</button>
<div class="collapse navbar-collapse" id="navbarNav">
<ul class="navbar-nav">
	<li class="nav-item">
		<a class="nav-link" href=<?=base_url()?>>Accueil</a>
	</li>
<?php if ($isloggedin === FALSE):?>
	<li class="nav-item">
		<a class="nav-link" href=<?=site_url("/login")?>>Connexion</a>
	</li>
<?php else:?>
		<li class="nav-item">
			<a class="nav-link" href=<?=site_url("/profile")?>>Mon profil</a>
		</li>


	<?php endif; ?>
	<li class="nav-item">
		<a class="nav-link" href=<?=site_url("/catalogue/console")?>>Console</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href=<?=site_url("/catalogue/jeux")?>>Jeux</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href=<?=site_url("/catalogue/accessoire")?>>Accessoire</a>
	</li>
<?php if ($isloggedin):?>
	<li class="nav-item">
		<a class="nav-link" href=<?=site_url("/welcome/log_out")?>>Déconnexion</a>
	</li>
<?php endif; ?>
</ul>
</div>
<form class="form-inline my-2 my-sm-0" accept-charset="utf-8" method='GET' action='<?= site_url("welcome/search")?>'>
	<input name='search' class="form-control mr-sm-1" type="search" placeholder='Rechercher'>
	<button class="btn btn-outline-success my-1 my-sm-0" type="submit">Recherche</button>
</form>
</nav>
